Please see [the Widget Extensions documentation](development/fe_guide/merge_request_widget_extensions.md) for necessary information regarding development of new MR Widgets.
