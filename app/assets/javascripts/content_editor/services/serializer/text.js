import { defaultMarkdownSerializer } from '~/lib/prosemirror_markdown_serializer';

const { text } = defaultMarkdownSerializer.nodes;

export default text;
